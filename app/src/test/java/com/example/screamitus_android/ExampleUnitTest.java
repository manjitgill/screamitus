package com.example.screamitus_android;

import org.junit.Assert;
import org.junit.Test;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

public class ExampleUnitTest {

    Infection infection = new Infection();


    // TC 1: to check that the number of days must be > 0
    @Test
    public void numberOfDays() {


        int numInfected = infection.calculateTotalInfected(0);
        Assert.assertEquals(1, numInfected);

    }

    // TC 2: The virus infect instructors at the rate of 5 per day
    @Test
    public void virusRate() {


        int numInfected = infection.calculateTotalInfected(5);
        Assert.assertEquals(25, numInfected);
    }

    // TC 3: The virus infect instructorsmore than 7 days at the rate of 8 per day after the 7 days
    @Test
    public void virusRateIfMoreThan7() {
        int numInfected = infection.calculateTotalInfected(9);
        Assert.assertEquals(51, numInfected);

    }
    // TC4:


}